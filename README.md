# ML DevOps Example

This repo shows an example of how to establish a DevOps pattern for Machine Learning. In particular, we use scikit-learn here to build a classifier on the simplified [PetFinder](https://www.kaggle.com/c/petfinder-adoption-prediction) dataset by the [Tensorflow team](https://www.tensorflow.org/tutorials/structured_data/feature_columns).

The simplified dataset can be obtained [here](http://storage.googleapis.com/download.tensorflow.org/data/petfinder-mini.zip).

The techniques employed here uses scikit-learn with the [sklearn2pmml](https://github.com/jpmml/sklearn2pmml) library to export a .pmml model file in the CI steps. The latest exported .pmml model can be obtained as a [job artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#access-the-latest-job-artifacts-by-url) by GitLab CI. In particular, the URL to the latest model is given [here](https://gitlab.com/markgeejw/ml-devops-example/-/jobs/artifacts/main/raw/PetFinder.pmml?job=build), if the artifact is not expired (30 days by default).

# License
[Apache License 2.0](LICENSE)